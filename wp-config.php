<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'notification_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ZUvwVth2;N~Xmq:mMylwGAaZk2.CURW :_SR>SlQ!ujaBd*<6xC6L>.K6_VQLxG.' );
define( 'SECURE_AUTH_KEY',  '3Wbj}}^+tZc3f;J{}8!~M#N+B{%%<aqs1732T/D|e/>VsGjut>zmgvh0`.v( Od*' );
define( 'LOGGED_IN_KEY',    'wf<8/js6/6EC%wf]ETkdkm^.@Fm}p,4SB2@o5A;b=/oBqcCR}vWqA?(HliCgjv4B' );
define( 'NONCE_KEY',        'k%$>0>+TK6&6ij-&5b$,?4@[QV;)~:IJy? Oz;hbR*0-%M<#F_3*=OT:DkcNQlx[' );
define( 'AUTH_SALT',        't &wQYqiIHLb@=]#ob^9j?fj-[AGobiJ~%NY8]qR^.c78qJhINmB,Rd]nYByU5%O' );
define( 'SECURE_AUTH_SALT', '$m,?a{!!djc9ee]fBa7<~Pwm4yAj`i#9HH>8Bs##Kqv^>+St;tK7xd]6|>$[?HZV' );
define( 'LOGGED_IN_SALT',   '{VX+6Ftkxk?X&v4`&(q=X0S4emg;/;?xL!OgK2S?%^0kK!N]ccP6$.HAY^SuaEUj' );
define( 'NONCE_SALT',       'J<#Qp>=R`Dx]T9DR3xf|Y9o_#0rG9[3xrpF=SmurE Ot|2(.dPr)hT)D+{HV[61N' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
